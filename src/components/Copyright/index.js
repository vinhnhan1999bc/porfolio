import React from 'react'

export default function Copyright() {
  return (
    <div className='copyright'>
      <p>Copyright © 2021 Nhanncv. All Right Reserved.</p>
    </div>
  )
}
